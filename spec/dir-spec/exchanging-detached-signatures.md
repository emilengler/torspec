# Exchanging detached signatures

Once an authority has computed and signed a consensus network status, it
should send its detached signature to each other authority in an HTTP POST
request to the URL:

`http://<hostname>/tor/post/consensus-signature`

\[XXX Note why we support push-and-then-pull.\]

All of the detached signatures it knows for consensus status should be
available at:

`http://<hostname>/tor/status-vote/next/consensus-signatures.z`

Assuming full connectivity, every authority should compute and sign the
same consensus including any flavors in each period.  Therefore, it
isn't necessary to download the consensus or any flavors of it computed
by each authority; instead, the authorities only push/fetch each
others' signatures.  A "detached signature" document contains items as
follows:

"consensus-digest" SP Digest NL

\[At start, at most once.\]

The digest of the consensus being signed.

"valid-after" SP YYYY-MM-DD SP HH:MM:SS NL
"fresh-until" SP YYYY-MM-DD SP HH:MM:SS NL
"valid-until" SP YYYY-MM-DD SP HH:MM:SS NL

\[As in the consensus\]

"additional-digest" SP flavor SP algname SP digest NL

\[Any number.\]

For each supported consensus flavor, every directory authority
adds one or more "additional-digest" lines.  "flavor" is the name
of the consensus flavor, "algname" is the name of the hash
algorithm that is used to generate the digest, and "digest" is the
hex-encoded digest.

The hash algorithm for the microdescriptor consensus flavor is
defined as SHA256 with algname "sha256".

```text
    "additional-signature" SP flavor SP algname SP identity SP
         signing-key-digest NL signature.

        [Any number.]
```

For each supported consensus flavor and defined digest algorithm,
every directory authority adds an "additional-signature" line.
"flavor" is the name of the consensus flavor.  "algname" is the
name of the algorithm that was used to hash the identity and
signing keys, and to compute the signature.  "identity" is the
hex-encoded digest of the authority identity key of the signing
authority, and "signing-key-digest" is the hex-encoded digest of
the current authority signing key of the signing authority.

The "sha256" signature format is defined as the RSA signature of
the OAEP+-padded SHA256 digest of the item to be signed.  When
checking signatures, the signature MUST be treated as valid if the
signature material begins with SHA256(document), so that other
data can get added later.
\[To be honest, I didn't fully understand the previous paragraph
and only copied it from the proposals.  Review carefully. -KL\]

"directory-signature"

\[As in the consensus; the signature object is the same as in the
consensus document.\]

<a id="dir-spec.txt-3.11"></a>
