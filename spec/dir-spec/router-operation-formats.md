<a id="dir-spec.txt-2"></a>

# Router operation and formats

This section describes how relays must behave when publishing their
information to the directory authorities, and the formats that they
use to do so.
