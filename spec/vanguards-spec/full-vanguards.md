# Full Vanguards

Full Vanguards is intended for use by long-lived onion services, which intend
to remain in operation for longer than one month.

Full Vanguards achieves this longer expected duration by having
two layers of additional fixed relays, of different rotation periods.

The rotation period of the first vanguard layer (layer 2 guards) is chosen
such that it requires an extremely long and persistent Sybil attack, or a
coercion/compromise attack.

The rotation period of the second vanguard layer (layer 3 guards) is chosen to
be small enough to force the adversary to perform a Sybil attack against this
layer, rather than attempting to coerce these relays.

## Threat model, Assumptions, and Goals

Consider an adversary with the following powers:

```text
     - Can launch a Sybil guard discovery attack against any position of a
       rendezvous circuit. The slower the rotation period of their target
       position, the longer the attack takes. Similarly, the higher the
       percentage of the network is controlled by them, the faster the
       attack runs.

     - Can compromise additional relays on the network, but this compromise
       takes time and potentially even coercive action, and also carries risk
       of discovery.
```

We also make the following assumptions about the types of attacks:

  1. A Sybil attack is observable by both people monitoring the network
     for large numbers of new relays, as well as vigilant hidden service
     operators. It will require large amounts of traffic sent towards the
     hidden service over many many test circuits.

  2. A Sybil attack requires either a protocol side channel or an application-layer
     timing side channel in order to determine successful placement next
     to the relay that the adversary is attempting to discover. When Tor's internal
     protocol side channels are dealt with, this will be both observable and
     controllable at the Application Layer, by operators.

  3. The adversary is strongly disincentivized from compromising additional
     relays that may prove useless, as active compromise attempts are even more
     risky for the adversary than a Sybil attack in terms of being noticed. In
     other words, the adversary is unlikely to attempt to compromise or coerce
     additional relays that are in use for only a short period of time.

Given this threat model, our security parameters were selected so that the
first two layers of guards should take a very long period of time to attack
using a Sybil guard discovery attack and hence require a relay compromise
attack.

On the other hand, the outermost layer of guards (the third layer) should
rotate fast enough to _require_ a Sybil attack. If the adversary were to
attempt to compromise or coerce these relays after they are discovered, their
rotation times should be fast enough that the adversary has a very high
probability of them no longer being in use.

# Design

When a hidden service picks its guard relays, it also picks an
additional NUM_LAYER2_GUARDS-sized set of middle relays for its
`second_guard_set`, as well as a NUM_LAYER3_GUARDS-sized set of
middle relays for its `third_guard_set`.

When a hidden service needs to establish a circuit to an HSDir,
introduction point or a rendezvous point, it uses relays from
`second_guard_set` as the second hop of the circuit and relays from
`third_guard_set` as third hop of the circuit.

A hidden service rotates relays from the 'second_guard_set' at a uniformly
random time between MIN_SECOND_GUARD_LIFETIME hours and
MAX_SECOND_GUARD_LIFETIME hours, chosen for each layer 2 relay.
Implementations MAY expose this layer as an explicit configuration option to
pin specific relays, similar to how a user is allowed to pin specific Guards.

A hidden service rotates relays from the 'third_guard_set' at a random time
between MIN_THIRD_GUARD_LIFETIME and MAX_THIRD_GUARD_LIFETIME hours, as
weighted by the [max(X,X)
distribution](./vanguards-stats.md#MaxDist), chosen for each
relay. This skewed distribution was chosen so that there is some probability
of a very short rotation period, to deter compromise/coercion, but biased
towards the longer periods, in favor of a somewhat lengthy Sybil attack. For
this reason, users SHOULD NOT be encouraged to pin this layer.

Each relay's rotation time is tracked independently, to avoid disclosing
the rotation times of the primary and second-level guards.

The selected vanguards and their rotation timestamp MUST be persisted to disk.

## Parameterization

We set NUM_LAYER2_GUARDS to 4 relays and NUM_LAYER3_GUARDS to 6 relays.

We set MIN_SECOND_GUARD_LIFETIME to 30 days, and MAX_SECOND_GUARD_LIFETIME to
60 days inclusive, for an average rotation rate of 45 days, using a uniform
distribution. This range was chosen to average out to half of the Guard
rotation period; there is no strong motivation for it otherwise, other than to
be long. In fact, it could be set as long as the Guard rotation, and longer
periods MAY be provided as a configuration parameter. 
 
From the [Sybil rotation table](./vanguards-stats.md#SybilTable) in [statistical
analysis](./vanguards-stats.md), with NUM_LAYER2_GUARDS=4, it
can be seen that this means that the Sybil attack on layer3 will complete with
50% chance in 18\*45 days (2.2 years) for the 1% adversary, 180 days for the
5% adversary, and 90 days for the 10% adversary, with a 45 day average
rotation period.

If this range is set equal to the Guard rotation period (90 days), the 50%
probability of Sybil success requires 18\*90 days (4.4 years) for the 1%
adversary, 4\*90 days (1 year) for the 5% adversary, and 2\*90 days (6 months)
for the 10% adversary.

We set MIN_THIRD_GUARD_LIFETIME to 1 hour, and MAX_THIRD_GUARD_LIFETIME to 48
hours inclusive, for an average rotation rate of 31.5 hours, using the
[max(X,X) distribution](./vanguards-stats.md#MaxDist).
(Again, this wide range and bias is used to discourage the adversary from
exclusively performing coercive attacks, as opposed to mounting the Sybil
attack, so increasing it substantially is not recommended).

From the [Sybil rotation
table](./vanguards-stats.md#SybilTable) in [statistical
analysis](./vanguards-stats.md), with NUM_LAYER3_GUARDS=6, it
can be seen that this means that the Sybil attack on layer3 will complete with
50% chance in 9\*31.5 hours (15.75 days) for the 1% adversary, ~4 days for the
5% adversary, and 2.62 days for the 10% adversary.

See the [statistical analysis](./vanguards-stats.md) for more
analysis on these constants.
